Images
======

Note Icon (https://thenounproject.com/term/note/51567/):
    Created by Marek Polakovic
    from the Noun Project (https://thenounproject.com/)
    Licensed under Creative Commons Attribution 3.0 US (http://creativecommons.org/licenses/by/3.0/us/)

Pencil Icon (https://thenounproject.com/term/pencil/5039/):
    Created by Dmitry Baranovskiy
    from the Noun Project (https://thenounproject.com/)
    Licensed under Creative Commons Attribution 3.0 US (http://creativecommons.org/licenses/by/3.0/us/)

Delete Icon (https://thenounproject.com/term/delete/6158/):
    Created by Thomas Le Bas
    from the Noun Project (https://thenounproject.com/)
    Licensed under Creative Commons Attribution 3.0 US (http://creativecommons.org/licenses/by/3.0/us/)

Zero Icon (https://thenounproject.com/term/off/80720/):
    Created by Arthur Shlain
    from the Noun Project (https://thenounproject.com/)
    Licensed under Creative Commons Attribution 3.0 US (http://creativecommons.org/licenses/by/3.0/us/)
