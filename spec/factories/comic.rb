# reqiure 'faker'

FactoryBot.define do
  factory :comic do |c|
    c.title { Faker::App.name }
    c.total_issues { 10 }
    c.url { Faker::Internet.url }
    c.issues_owned { '2,4-7,9' }
    c.next_issue_to_read { 0 }
    c.has_issue_zero { true }
    c.user_id { 1 }
    c.no_more_issues { false }
    c.notes { 'Some words' }
    c.issues_removed_from_storage { '' }
  end
end
