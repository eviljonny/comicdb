require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.safe_email }
    password { 'qwwrfer4edc' }
    password_confirmation { 'qwwrfer4edc' }
  end
end
