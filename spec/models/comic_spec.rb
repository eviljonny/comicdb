require 'rails_helper'

describe Comic do
  let(:user) { FactoryBot.create :user }

  def build_comic(**kwargs)
    FactoryBot.build(:comic, user_id: user.id, **kwargs)
  end

  it 'has a valid factory' do
    expect(build_comic).to be_valid
  end

  context 'title' do
    it 'is invalid when nil' do
      expect(build_comic(title: nil)).to be_invalid
    end

    it 'is invalid when an empty string' do
      expect(build_comic(title: '')).to be_invalid
    end
  end

  context 'total number of issues' do
    it 'is invalid when nil' do
      expect(build_comic(total_issues: nil)).to be_invalid
    end

    it 'is invalid when negative' do
      expect(build_comic(total_issues: -1)).to be_invalid
    end

    it 'is invalid when a float' do
      expect(build_comic(total_issues: 2.1)).to be_invalid
    end

    it 'is invalid when a string' do
      expect(build_comic(total_issues: 'a')).to be_invalid
    end
  end

  context 'has issue zero' do
    it 'is valid when false' do
      expect(build_comic(has_issue_zero: false)).to be_valid
    end

    it 'is valid when true' do
      expect(build_comic(has_issue_zero: true)).to be_valid
    end

    it 'is invalid if missing' do
      expect(build_comic(has_issue_zero: nil)).to be_invalid
    end
  end

  context 'next issue to read' do
    it 'is invalid when nil' do
      expect(build_comic(next_issue_to_read: nil)).to be_invalid
    end

    it 'is invalid when negative' do
      expect(build_comic(next_issue_to_read: -1)).to be_invalid
    end

    it 'is invalid when a float' do
      expect(build_comic(next_issue_to_read: 2.1)).to be_invalid
    end

    it 'is invalid when a string' do
      expect(build_comic(next_issue_to_read: 'a')).to be_invalid
    end
  end

  context 'issues owned' do
    it 'is valid when empty' do
      expect(build_comic(issues_owned: '')).to be_valid
    end

    it 'is valid when nil' do
      expect(build_comic(issues_owned: nil)).to be_valid
    end

    it 'is valid when a single int in a string' do
      expect(build_comic(issues_owned: '1')).to be_valid
    end

    it 'is valid when ints separated with commas in a string' do
      expect(build_comic(issues_owned: '1,2')).to be_valid
    end

    it 'is valid when it is a range' do
      expect(build_comic(issues_owned: '1-3')).to be_valid
    end

    it 'is valid when a mix of ints and ranges' do
      expect(build_comic(issues_owned: '1,2-5,7')).to be_valid
    end

    it 'is invalid when a letter' do
      expect(build_comic(issues_owned: 'a')).to be_invalid
    end

    it 'is invalid when a range starts higher than it ends' do
      expect(build_comic(issues_owned: '11-10')).to be_invalid
    end

    it 'is invalid when a range starts and ends the same' do
      expect(build_comic(issues_owned: '10-10')).to be_invalid
    end
  end

  context 'issues_removed_from_storage' do
    def build_removed_issues_comic(has_issue_zero: true, issues_owned: '0-10', **kwargs)
      build_comic(issues_owned: issues_owned, has_issue_zero: has_issue_zero, **kwargs)
    end

    it 'is valid when empty' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '')).to be_valid
    end

    it 'is valid when nil' do
      expect(build_removed_issues_comic(issues_removed_from_storage: nil)).to be_valid
    end

    it 'is valid when a single int in a string' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '1')).to be_valid
    end

    it 'is valid when ints separated with commas in a string' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '1,2')).to be_valid
    end

    it 'is valid when it is a range' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '1-3')).to be_valid
    end

    it 'is valid when a mix of ints and ranges' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '1,2-5,7')).to be_valid
    end

    it 'is invalid when a letter' do
      expect(build_removed_issues_comic(issues_removed_from_storage: 'a')).to be_invalid
    end

    it 'is invalid when a range starts higher than it ends' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '11-10')).to be_invalid
    end

    it 'is invalid when a range starts and ends the same' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '10-10')).to be_invalid
    end

    it 'is invalid when there are issues removed from storage which are not owned' do
      expect(build_removed_issues_comic(issues_removed_from_storage: '3-7', issues_owned: '8-10')).to be_invalid
    end
  end

  context 'no_more_issues' do
    it 'is valid when false' do
      expect(build_comic(no_more_issues: false)).to be_valid
    end

    it 'is valid when true' do
      expect(build_comic(no_more_issues: true)).to be_valid
    end

    it 'is invalid if missing' do
      expect(build_comic(no_more_issues: nil)).to be_invalid
    end
  end

  context 'notes' do
    it 'is valid when nil' do
      expect(build_comic(notes: nil)).to be_valid
    end

    it 'is valid when empty' do
      expect(build_comic(notes: '')).to be_valid
    end

    it 'is valid when it has one line of text' do
      expect(build_comic(notes: 'Here is some text')).to be_valid
    end

    it 'is valid with multiple lines of text' do
      expect(build_comic(notes: "Here is some text\nand another line")).to be_valid
    end
  end

  context '#first_issue_in_series' do
    subject(:comic) do
      build_comic(
        has_issue_zero: has_issue_zero
      )
    end

    context 'when the series has issue zero' do
      let(:has_issue_zero) { true }

      it { expect(comic.first_issue_in_series).to eq 0 }
    end

    context 'when the series does not have issue zero' do
      let(:has_issue_zero) { false }

      it { expect(comic.first_issue_in_series).to eq 1 }
    end
  end

  context '#started_reading?' do
    subject(:comic) do
      build_comic(
        next_issue_to_read: next_issue_to_read,
        issues_owned: issues_owned,
        has_issue_zero: has_issue_zero
      )
    end

    context 'when we do not have any issues' do
      let(:issues_owned) { '' }
      let(:has_issue_zero) { false }

      context 'when the next issue to read is 0' do
        let(:next_issue_to_read) { 0 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is 1' do
        let(:next_issue_to_read) { 1 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is 2' do
        let(:next_issue_to_read) { 2 }

        it { expect(comic.started_reading?).to be false }
      end
    end

    context 'when we have the series from the start' do
      context 'when the comic has a zero issue' do
        let(:issues_owned) { '0-5' }
        let(:has_issue_zero) { true }

        context 'when the next issue to read is 0' do
          let(:next_issue_to_read) { 0 }

          it { expect(comic.started_reading?).to be false }
        end

        context 'when the next issue to read is 1' do
          let(:next_issue_to_read) { 1 }

          it { expect(comic.started_reading?).to be true }
        end

        context 'when the next issue to read is 2' do
          let(:next_issue_to_read) { 2 }

          it { expect(comic.started_reading?).to be true }
        end
      end

      context 'when the comic does not have a zero issue' do
        let(:issues_owned) { '1-5' }
        let(:has_issue_zero) { false }

        context 'when the next issue to read is 0' do
          let(:next_issue_to_read) { 0 }

          it { expect(comic.started_reading?).to be false }
        end

        context 'when the next issue to read is 1' do
          let(:next_issue_to_read) { 1 }

          it { expect(comic.started_reading?).to be false }
        end

        context 'when the next issue to read is 2' do
          let(:next_issue_to_read) { 2 }

          it { expect(comic.started_reading?).to be true }
        end
      end
    end

    context 'when we do not have the earliest episodes' do
      let(:issues_owned) { '25-30' }
      let(:has_issue_zero) { false }

      context 'when the next issue to read is 0' do
        let(:next_issue_to_read) { 0 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is one less than the first issue owned' do
        let(:next_issue_to_read) { 24 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is exactly the first issue owned' do
        let(:next_issue_to_read) { 25 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is higher than the first issue owned' do
        let(:next_issue_to_read) { 26 }

        it { expect(comic.started_reading?).to be true }
      end
    end
  end

  context '#finished_reading?' do
    subject(:comic) do
      build_comic(
        total_issues: 10,
        next_issue_to_read: next_issue_to_read,
        issues_owned: issues_owned,
        has_issue_zero: false
      )
    end

    context 'when we do not have any issues' do
      let(:issues_owned) { '' }

      context 'and the next issue to read is 0' do
        let(:next_issue_to_read) { 0 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is 1' do
        let(:next_issue_to_read) { 1 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is exactly total issues' do
        let(:next_issue_to_read) { 10 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is above the total issues' do
        let(:next_issue_to_read) { 11 }

        it { expect(comic.finished_reading?).to be false }
      end
    end

    context 'when we have all the issues' do
      let(:issues_owned) { '1-10' }

      context 'and the next issue to read is 0' do
        let(:next_issue_to_read) { 0 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is 1' do
        let(:next_issue_to_read) { 1 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is exactly total issues' do
        let(:next_issue_to_read) { 10 }

        it { expect(comic.finished_reading?).to be false }
      end

      context 'and the next issue to read is above the total issues' do
        let(:next_issue_to_read) { 11 }

        it { expect(comic.finished_reading?).to be true }
      end
    end

    context 'when there is an issue missing' do
      context 'and that issue is in the middle' do
        let(:issues_owned) { '1-4,10' }

        context 'and the next issue to read is 0' do
          let(:next_issue_to_read) { 0 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is 1' do
          let(:next_issue_to_read) { 1 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is exactly total issues' do
          let(:next_issue_to_read) { 10 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is above the total issues' do
          let(:next_issue_to_read) { 11 }

          it { expect(comic.finished_reading?).to be true }
        end
      end

      context 'and that issue is the last one' do
        let(:issues_owned) { '1-9' }

        context 'and the next issue to read is 0' do
          let(:next_issue_to_read) { 0 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is 1' do
          let(:next_issue_to_read) { 1 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is exactly total issues' do
          let(:next_issue_to_read) { 10 }

          it { expect(comic.finished_reading?).to be false }
        end

        context 'and the next issue to read is above the total issues' do
          let(:next_issue_to_read) { 11 }

          it { expect(comic.finished_reading?).to be true }
        end
      end
    end
  end

  describe "#first_issue_owned" do
    subject(:comic) do
      build_comic(
        issues_owned: issues_owned,
        has_issue_zero: has_issue_zero
      )
    end
    let(:has_issue_zero) { false }

    context "when the issues owned is empty" do
      let(:issues_owned) { '' }

      it "returns -1" do
        expect(comic.first_issue_owned).to eq(-1)
      end
    end

    context "when the series has an issue zero" do
      let(:has_issue_zero) { true }

      context "and we own issue zero" do
        let(:issues_owned) { '0-10' }

        it "returns 0" do
          expect(comic.first_issue_owned).to eq 0
        end
      end

      context "and we do not own issue zero" do
        let(:issues_owned) { '1-10' }

        it "returns the first issue owned" do
          expect(comic.first_issue_owned).to eq 1
        end
      end
    end

    context "when the issues owned is just a single number" do
      let(:issues_owned) { '5' }

      it "returns the first issue owned" do
        expect(comic.first_issue_owned).to eq 5
      end
    end

    context "when the issues owned is a combination of number then range" do
      let(:issues_owned) { '5,7-10' }

      it "returns the first issue owned" do
        expect(comic.first_issue_owned).to eq 5
      end
    end

    context "when the issues owned is a combination of range then number" do
      let(:issues_owned) { '5-7,10' }

      it "returns the first issue owned" do
        expect(comic.first_issue_owned).to eq 5
      end
    end

    context "when the issues owned is just a range" do
      let(:issues_owned) { '5-7' }

      it "returns the first issue owned" do
        expect(comic.first_issue_owned).to eq 5
      end
    end
  end

  describe "#last_issue_owned" do
    subject(:comic) do
      build_comic(
        issues_owned: issues_owned
      )
    end

    context "when the issues owned is empty" do
      let(:issues_owned) { '' }

      it "returns -1" do
        expect(comic.last_issue_owned).to eq(-1)
      end
    end

    context "when the issues owned is just a single number" do
      let(:issues_owned) { '5' }

      it "returns the first issue owned" do
        expect(comic.last_issue_owned).to eq 5
      end
    end

    context "when the issues owned is a combination of number then range" do
      let(:issues_owned) { '5,7-10' }

      it "returns the first issue owned" do
        expect(comic.last_issue_owned).to eq 10
      end
    end

    context "when the issues owned is a combination of range then number" do
      let(:issues_owned) { '5-7,10' }

      it "returns the first issue owned" do
        expect(comic.last_issue_owned).to eq 10
      end
    end

    context "when the issues owned is just a range" do
      let(:issues_owned) { '5-7' }

      it "returns the first issue owned" do
        expect(comic.last_issue_owned).to eq 7
      end
    end
  end

  context "#more_to_read?" do
    subject(:comic) do
      build_comic(
        next_issue_to_read: next_issue_to_read,
        issues_owned: issues_owned
      )
    end

    context 'when we do not have any issues' do
      let(:issues_owned) { '' }

      context 'when the next issue to read is -1' do
        let(:next_issue_to_read) { -1 }

        it { expect(comic.started_reading?).to be false }
      end

      context 'when the next issue to read is 1' do
        let(:next_issue_to_read) { 1 }

        it { expect(comic.started_reading?).to be false }
      end
    end

    context 'when we have issues' do
      let(:issues_owned) { '5-10' }

      context "and the next issue to read is before the issues we have" do
        let(:next_issue_to_read) { 0 }

        it { expect(comic.more_to_read?).to be true }
      end

      context "and the next issue to read is the first of the issues we have" do
        let(:next_issue_to_read) { 5 }

        it { expect(comic.more_to_read?).to be true }
      end

      context "and the next issue to read is within the issues we have" do
        let(:next_issue_to_read) { 7 }

        it { expect(comic.more_to_read?).to be true }
      end

      context "and the next issue to read is exactly the last issue we have" do
        let(:next_issue_to_read) { 10 }

        it { expect(comic.more_to_read?).to be true }
      end

      context "and the next issue to read is after the last issue we have" do
        let(:next_issue_to_read) { 11 }

        it { expect(comic.more_to_read?).to be false }
      end
    end
  end

  describe "#all_issues_owned?" do
    subject(:comic) do
      build_comic(
        issues_owned: issues_owned,
        total_issues: 10,
        has_issue_zero: has_issue_zero
      )
    end
    let(:has_issue_zero) { false }

    context "when we don't own any issues" do
      let(:issues_owned) { "" }

      it { expect(comic.all_issues_owned?).to be false }
    end

    context "when the series has an issue zero" do
      let(:has_issue_zero) { true }

      context "and we own all but issue zero" do
        let(:issues_owned) { "1-10" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and we own only issue zero" do
        let(:issues_owned) { "0" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and we own all the issues" do
        let(:issues_owned) { "0-10" }

        it { expect(comic.all_issues_owned?).to be true }
      end
    end

    context "when we own all the issues" do
      context "and the issues_owned is a single range" do
        let(:issues_owned) { "1-10" }

        it { expect(comic.all_issues_owned?).to be true }
      end

      context "and the issues_owned is multiple ranges" do
        let(:issues_owned) { "1-5,6-10" }

        it { expect(comic.all_issues_owned?).to be true }
      end

      context "and the issues_owned is multiple overlapping ranges" do
        let(:issues_owned) { "1-5,4-10" }

        it { expect(comic.all_issues_owned?).to be true }
      end

      context "and the issues owned is a series of single ints" do
        let(:issues_owned) { "1,2,3,4,5,6,7,8,9,10" }

        it { expect(comic.all_issues_owned?).to be true }
      end

      context "and the issues owned is a mix of single ints and ranges" do
        let(:issues_owned) { "1-5,6,7-9,10" }

        it { expect(comic.all_issues_owned?).to be true }
      end
    end

    context "when we own only one issue" do
      context "and it's the first issue" do
        let(:issues_owned) { "1" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and it's an issue in the middle" do
        let(:issues_owned) { "5" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and it's the last issue" do
        let(:issues_owned) { "10" }

        it { expect(comic.all_issues_owned?).to be false }
      end
    end

    context "when we own all but one of the issues" do
      context "and it's the first issue" do
        let(:issues_owned) { "2-10" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and it's an issue in the middle" do
        let(:issues_owned) { "1-4,6-10" }

        it { expect(comic.all_issues_owned?).to be false }
      end

      context "and it's the last issue" do
        let(:issues_owned) { "1-9" }

        it { expect(comic.all_issues_owned?).to be false }
      end
    end
  end

  describe "#next_issue_available?" do
    subject(:comic) do
      build_comic(
        issues_owned: issues_owned,
        total_issues: 10,
        has_issue_zero: has_issue_zero,
        next_issue_to_read: next_issue_to_read
      )
    end
    let(:has_issue_zero) { false }
    let(:next_issue_to_read) { 0 }
    let(:issues_owned) { "1-10" }

    context "when we have no issues" do
      let(:issues_owned) { "" }

      it { expect(comic.next_issue_available?).to be false }
    end

    context "when the next episode is the start of the series" do
      context "and the series has a zero issue" do
        let(:has_issue_zero) { true }

        context "and next_issue_to_read is available" do
          let(:issues_owned) { "0-10" }

          context "and next_issue_to_read is exactly the first issue" do
            let(:next_issue_to_read) { 0 }

            it { expect(comic.next_issue_available?).to be true }
          end

          context "and next_issue_to_read is one below the first issue" do
            let(:next_issue_to_read) { -1 }

            it { expect(comic.next_issue_available?).to be true }
          end
        end

        context "and next_issue_to_read is unavailable" do
          let(:issues_owned) { "1-10" }

          context "and next_issue_to_read is exactly the first issue" do
            let(:next_issue_to_read) { 0 }

            it { expect(comic.next_issue_available?).to be false }
          end

          context "and next_issue_to_read is one below the first issue" do
            let(:next_issue_to_read) { -1 }

            it { expect(comic.next_issue_available?).to be false }
          end
        end
      end

      context "and the series does not have an issue zero" do
        let(:has_issue_zero) { false }

        context "and next_issue_to_read is available" do
          let(:issues_owned) { "1-10" }

          context "and next_issue_to_read is exactly the first issue" do
            let(:next_issue_to_read) { 1 }

            it { expect(comic.next_issue_available?).to be true }
          end

          context "and next_issue_to_read is one below the first issue" do
            let(:next_issue_to_read) { 0 }

            it { expect(comic.next_issue_available?).to be true }
          end
        end

        context "and next_issue_to_read is unavailable" do
          let(:issues_owned) { "2-10" }

          context "and next_issue_to_read is exactly the first issue" do
            let(:next_issue_to_read) { 1 }

            it { expect(comic.next_issue_available?).to be false }
          end

          context "and next_issue_to_read is one below the first issue" do
            let(:next_issue_to_read) { 0 }

            it { expect(comic.next_issue_available?).to be false }
          end
        end
      end
    end

    context "when the next episode is mid series" do
      context "and next_issue_to_read is firmly in the middle" do
        let(:next_issue_to_read) { 5 }

        context "and next_issue_to_read is available" do
          it { expect(comic.next_issue_available?).to be true }
        end

        context "and next_issue_to_read is not available" do
          let(:issues_owned) { "1-4,6-10" }

          it { expect(comic.next_issue_available?).to be false }
        end
      end

      context "and next_issue_to_read is exactly the last episode of the series" do
        let(:next_issue_to_read) { 10 }

        context "and next_issue_to_read is available" do
          it { expect(comic.next_issue_available?).to be true }
        end

        context "and next_issue_to_read is not available" do
          let(:issues_owned) { "1-9" }

          it { expect(comic.next_issue_available?).to be false }
        end
      end
    end

    context "when the next episode is after the series" do
      let(:next_issue_to_read) { 11 }

      it { expect(comic.next_issue_available?).to be false }
    end
  end

  context "#any_issues_removed_from_storage?" do
    def build_removed_comic(has_issue_zero: false, issues_owned: '0-10', **kwargs)
      build_comic(issues_owned: issues_owned, has_issue_zero: has_issue_zero, **kwargs)
    end

    context "when we own issue zero" do
      context "and issue zero has been removed from storage" do
        it "is true" do
          expect(build_removed_comic(
            has_issue_zero: true,
            issues_removed_from_storage: '0'
          ).any_issues_removed_from_storage?).
            to be true
        end
      end

      context "and issue zero has not been removed from storage" do
        it "is false" do
          expect(build_removed_comic(
            has_issue_zero: true,
            issues_removed_from_storage: ''
          ).any_issues_removed_from_storage?).
            to be false
        end
      end
    end

    it 'is false when empty' do
      expect(build_removed_comic(issues_removed_from_storage: '').any_issues_removed_from_storage?).to be false
    end

    it 'is false when nil' do
      expect(build_removed_comic(issues_removed_from_storage: nil).any_issues_removed_from_storage?).to be false
    end

    it 'is true when a single int in a string' do
      expect(build_removed_comic(issues_removed_from_storage: '1').any_issues_removed_from_storage?).to be true
    end

    it 'is true when ints separated with commas in a string' do
      expect(build_removed_comic(issues_removed_from_storage: '1,2').any_issues_removed_from_storage?).to be true
    end

    it 'is true when it is a range' do
      expect(build_removed_comic(issues_removed_from_storage: '1-3').any_issues_removed_from_storage?).to be true
    end

    it 'is true when a mix of ints and ranges' do
      expect(build_removed_comic(issues_removed_from_storage: '1,2-5,7').any_issues_removed_from_storage?).to be true
    end
  end

  context "#number_of_issues_read" do
    let(:comic) do
      build_comic(has_issue_zero: has_issue_zero, next_issue_to_read: next_issue_to_read, issues_owned: issues_owned)
    end
    let(:next_issue_to_read) { 4 }

    context "when it has a zero issue" do
      let(:has_issue_zero) { true }

      context "when all issues prior to next_issue_to_read are owned" do
        let(:issues_owned) { '0-10' }

        it { expect(comic.number_of_issues_read).to eq 4 }
      end

      context "when some issues prior to next_issue_to_read are missing" do
        context "when issue 0 is one that is missing" do
          let(:issues_owned) { '1-10' }

          it { expect(comic.number_of_issues_read).to eq 3 }
        end

        context "when issue 0 isn't one that is missing" do
          let(:issues_owned) { '0-2,4-10' }

          it { expect(comic.number_of_issues_read).to eq 3 }
        end
      end

      context "when no issues prior to next_issue_to_read are owned" do
        let(:issues_owned) { '4-10' }

        it { expect(comic.number_of_issues_read).to eq 0 }
      end

      context "when next_issue_to_read is 0 and we own all the issues" do
        let(:issues_owned) { '0-10' }
        let(:next_issue_to_read) { 0 }

        it { expect(comic.number_of_issues_read).to eq 0 }
      end

      context "when next_issue_to_read is 1 and we own all the issues" do
        let(:issues_owned) { '0-10' }
        let(:next_issue_to_read) { 1 }

        it { expect(comic.number_of_issues_read).to eq 1 }
      end
    end

    context "when it does not have an issue zero" do
      let(:has_issue_zero) { false }

      context "when all issues prior to next_issue_to_read are owned" do
        let(:issues_owned) { '1-10' }

        it { expect(comic.number_of_issues_read).to eq 3 }
      end

      context "when some issues prior to next_issue_to_read are missing" do
        let(:issues_owned) { '1-2,4-10' }

        it { expect(comic.number_of_issues_read).to eq 2 }
      end

      context "when no issues prior to next_issue_to_read are owned" do
        let(:issues_owned) { '1-2,4-10' }

        it { expect(comic.number_of_issues_read).to eq 2 }
      end

      context "when next issue to read is higher than total issues" do
        context "when it is 1 higher" do
          let(:next_issue_to_read) { 11 }
          let(:issues_owned) { "1-10" }

          it { expect(comic.number_of_issues_read).to eq 10 }
        end

        context "when it is 2 higher" do
          let(:next_issue_to_read) { 12 }
          let(:issues_owned) { "1-10" }

          it { expect(comic.number_of_issues_read).to eq 10 }
        end
      end

      context "when no issues are owned" do
        context "and next_issue_to_read is 0" do
          let(:next_issue_to_read) { 0 }
          let(:issues_owned) { "" }

          it { expect(comic.number_of_issues_read).to eq 0 }
        end

        context "and next_issue_to_read is 11" do
          let(:next_issue_to_read) { 11 }
          let(:issues_owned) { "" }

          it { expect(comic.number_of_issues_read).to eq 0 }
        end
      end

      context "when next_issue_to_read is 0 and we own all the issues" do
        let(:issues_owned) { '1-10' }
        let(:next_issue_to_read) { 0 }

        it { expect(comic.number_of_issues_read).to eq 0 }
      end

      context "when next_issue_to_read is 1 and we own all the issues" do
        let(:issues_owned) { '1-10' }
        let(:next_issue_to_read) { 1 }

        it { expect(comic.number_of_issues_read).to eq 0 }
      end
    end
  end
end
