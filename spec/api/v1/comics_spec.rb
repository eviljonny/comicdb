require 'rails_helper'
require 'faker'
require 'json'

describe 'Comics API' do
  let(:base_path) { '/api/v1/comics/' }

  let(:user) { FactoryBot.create :user }
  let(:comics) { FactoryBot.create_list(:comic, 3, user_id: user.id) }

  let(:user_auth_headers) do
    {
      'X-User-Email' => user.email,
      'X-User-Token' => user.authentication_token
    }
  end

  let(:other_user) { FactoryBot.create :user }
  let(:other_comics) { FactoryBot.create_list(:comic, 5, user_id: other_user.id) }

  let(:not_found_error) do
    {
      'error' => 'Comic not found'
    }
  end

  before(:each) do
    user.confirm
    other_user.confirm

    user.comics = comics
    user.save!

    other_user.comics = other_comics
    other_user.save!
  end

  def get_with_auth_headers(url, params: nil, headers: {})
    get url, params: params, headers: user_auth_headers.merge(headers)
  end

  def decode_body
    JSON.parse(response.body)
  end

  def expect_unauthorized_body
    expect(decode_body).to eq unauthorized_body
  end

  context 'When unauthenticated' do
    let(:unauthorized_body) do
      {
        'error' => 'You need to sign in or sign up before continuing.'
      }
    end

    it 'fails with no email or auth token' do
      get base_path

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with no email header' do
      user_auth_headers.delete 'X-User-Email'
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with an empty email header' do
      user_auth_headers['X-User-Email'] = ''
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with an unrecognised email header' do
      user_auth_headers['X-User-Email'] = Faker::Internet.safe_email
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with no auth token' do
      user_auth_headers.delete 'X-User-Token'
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with an incorrect auth token' do
      user_auth_headers['X-User-Token'] = 'NOPE'
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end

    it 'fails with an empty auth token' do
      user_auth_headers['X-User-Token'] = ''
      get base_path, params: nil, headers: user_auth_headers

      expect(response).to be_unauthorized
      expect_unauthorized_body
    end
  end

  context 'With token auth' do
    it 'can get the index' do
      get_with_auth_headers base_path

      expect(response).to be_successful
    end

    context 'comic list' do
      it 'gets the correct list' do
        get_with_auth_headers base_path

        expect(response).to be_successful
        expect(decode_body).to eq JSON.parse(comics.sort_by(&:title).to_json)
      end
    end

    context 'get a single comic' do
      it 'gets a comic' do
        get_with_auth_headers base_path + "#{comics.first.id}"

        expect(response).to be_successful
        expect(decode_body).to eq JSON.parse(comics.first.to_json)
      end

      it 'cannnot get another users comic' do
        get_with_auth_headers base_path + "#{other_comics.first.id}"

        expect(response).to be_not_found
        expect(decode_body).to eq not_found_error
      end

      it 'returns a not found error for non-existing comic' do
        get_with_auth_headers base_path + '0'

        expect(response).to be_not_found
        expect(decode_body).to eq not_found_error
      end
    end
  end
end
