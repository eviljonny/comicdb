require 'rails_helper'

RSpec.describe Comicdb::Recommenders::CompleteUnstartedSeriesRecommender do
  subject(:recommender) { described_class.new }

  def build_comic(issues_owned: "1-10", next_issue: 1, no_more_issues: true)
    FactoryBot.build(
      :comic,
      total_issues: 10,
      issues_owned: issues_owned,
      next_issue_to_read: next_issue,
      no_more_issues: no_more_issues,
      has_issue_zero: false
    )
  end

  context "when there is a series which is complete and unstarted only" do
    let(:comics) { [build_comic] }

    it { expect(recommender.recommendations_for(comics)).to eq [comics[0]] }
  end

  context "when there is a series which is complete and unstarted among others" do
    let(:comics) do
      [
        build_comic(issues_owned: "1-4,6-10"),
        build_comic,
        build_comic(issues_owned: "1-9")
      ]
    end

    it { expect(recommender.recommendations_for(comics)).to eq [comics[1]] }
  end

  context "when there is a series which is complete and unstarted but there are more issues left" do
    let(:comics) { [build_comic(no_more_issues: false)] }

    it { expect(recommender.recommendations_for(comics)).to be_empty }
  end

  context "when there is a series which is complete but started" do
    let(:comics) { [build_comic(next_issue: 5)] }

    it { expect(recommender.recommendations_for(comics)).to be_empty }
  end

  context "when there is a series which is incomplete and unstarted" do
    let(:comics) { [build_comic(issues_owned: '1-4,6-10')] }

    it { expect(recommender.recommendations_for(comics)).to be_empty }
  end

  context "when there is a series which is incomplete but started" do
    let(:comics) { [build_comic(issues_owned: '1-4,6-10', next_issue: 5)] }

    it { expect(recommender.recommendations_for(comics)).to be_empty }
  end
end
