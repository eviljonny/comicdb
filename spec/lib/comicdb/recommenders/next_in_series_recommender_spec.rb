require 'rails_helper'

RSpec.describe Comicdb::Recommenders::NextInSeriesRecommender do
  subject(:recommender) { described_class.new }

  def build_comic(issues_owned: "1-10", next_issue: 5)
    FactoryBot.build(
      :comic,
      total_issues: 10,
      issues_owned: issues_owned,
      next_issue_to_read: next_issue
    )
  end

  context "When we are mid reading no series" do
    context "and there is a series waiting to be started" do
      let(:comics) { [build_comic(next_issue: 1)] }

      it { expect(recommender.recommendations_for(comics)).to be_empty }
    end

    context "and there is a series which has been completed" do
      let(:comics) { [build_comic(next_issue: 11)] }

      it { expect(recommender.recommendations_for(comics)).to be_empty }
    end
  end

  context "When we are mid reading a series" do
    context "and there is a series where the next episode is missing" do
      let(:comics) { [build_comic(issues_owned: "1-4,6-10", next_issue: 5)] }

      it { expect(recommender.recommendations_for(comics)).to be_empty }
    end

    context "and there is a series which we have the next episode" do
      let(:comics) { [build_comic] }

      it { expect(recommender.recommendations_for(comics)).to eq [comics[0]] }
    end

    context "and there are a mix of series which should and shouldn't be recommended" do
      let(:comics) do
        [
          build_comic(next_issue: 11),
          build_comic,
          build_comic(next_issue: 1),
          build_comic(issues_owned: "1-7,9-10", next_issue: 8),
          build_comic
        ]
      end

      it { expect(recommender.recommendations_for(comics)).to eq [comics[1], comics[4]] }
    end
  end
end
