require 'rails_helper'

RSpec.describe Comicdb::Recommenders::MissingIssuesRecommender do
  subject(:recommender) { described_class.new }

  def build_comic(has_issue_zero: false, issues_owned: "1-10")
    FactoryBot.build(
      :comic,
      total_issues: 10,
      issues_owned: issues_owned,
      has_issue_zero: has_issue_zero
    )
  end

  context "when there is an issue zero" do
    context "and there are only comics with no missing issues" do
      let(:comics) do
        [
          build_comic(has_issue_zero: true, issues_owned: "0-10"),
          build_comic(has_issue_zero: true, issues_owned: "0-10")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq [] }
    end

    context "and there are a mix of comics with issues missing and not" do
      let(:comics) do
        [
          build_comic(has_issue_zero: true, issues_owned: "1-10"),
          build_comic(has_issue_zero: true, issues_owned: "0-10")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq [comics[0]] }
    end

    context "and there only comics with missing issues" do
      let(:comics) do
        [
          build_comic(has_issue_zero: true, issues_owned: "1-10"),
          build_comic(has_issue_zero: true, issues_owned: "0-9")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq comics }
    end
  end

  context "when there is not an issue zero" do
    let(:has_issue_zero) { false }

    context "and there are only comics with no missing issues" do
      let(:comics) do
        [
          build_comic(issues_owned: "0-10"),
          build_comic(issues_owned: "1-10")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq [] }
    end

    context "and there are a mix of comics with issues missing and not" do
      let(:comics) do
        [
          build_comic(issues_owned: "1-9"),
          build_comic(issues_owned: "0-10")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq [comics[0]] }
    end

    context "and there only comics with missing issues" do
      let(:comics) do
        [
          build_comic(issues_owned: "2-10"),
          build_comic(issues_owned: "0-9")
        ]
      end

      it { expect(subject.recommendations_for(comics)).to eq comics }
    end
  end
end
