require 'rails_helper'

RSpec.describe Comicdb::Recommenders::IssuesRemovedFromStorageRecommender do
  subject(:recommender) { described_class.new }

  def build_comic(removed: "")
    FactoryBot.build(
      :comic,
      total_issues: 10,
      issues_owned: issues_owned,
      has_issue_zero: has_issue_zero,
      issues_removed_from_storage: removed
    )
  end

  context "when there is an issue zero" do
    let(:has_issue_zero) { true }

    context "and we own issue zero" do
      let(:issues_owned) { "0-10" }

      context "and issue zero has been removed from storage" do
        let(:comics) do
          [ build_comic(removed: '0') ]
        end

        it { expect(subject.recommendations_for(comics)).to eq comics }
      end

      context "and issue zero has not been removed from storage" do
        let(:comics) do
          [ build_comic ]
        end

        it { expect(subject.recommendations_for(comics)).to eq [] }
      end
    end

    context "and we don't own issue zero" do
      let(:issues_owned) { "1-10" }

      context "and issue zero has not been removed form storage" do
        context "and no other issues have been removed from storage" do
          let(:comics) do
            [ build_comic, build_comic ]
          end

          it { expect(subject.recommendations_for(comics)).to eq [] }
        end

        context "and another comic has been removed from storage" do
          let(:comics) do
            [ build_comic, build_comic(removed: '3-5') ]
          end

          it { expect(subject.recommendations_for(comics)).to eq [comics[1]] }
        end
      end
    end
  end

  context "when there is no issue zero" do
    let(:has_issue_zero) { false }
    let(:issues_owned) { '1-10' }

    context "and there is just one comic" do
      context "and that comic has issues that have been removed from storage" do
        let(:comics) do
          [ build_comic(removed: '3-5') ]
        end

        it { expect(subject.recommendations_for(comics)).to eq comics }
      end

      context "and that comic has not been removed from storage" do
        let(:comics) do
          [ build_comic ]
        end

        it { expect(subject.recommendations_for(comics)).to eq [] }
      end
    end

    context "when there are multiple comics" do
      context "and none have been removed from storage" do
        let(:comics) do
          [ build_comic, build_comic, build_comic ]
        end

        it { expect(subject.recommendations_for(comics)).to eq [] }
      end

      context "and one has been removed from storage" do
        let(:comics) do
          [ build_comic, build_comic, build_comic(removed: '5') ]
        end

        it { expect(subject.recommendations_for(comics)).to eq [comics[2]] }
      end

      context "and several have been removed from storage" do
        let(:comics) do
          [ build_comic(removed: '1-10'), build_comic, build_comic(removed: '5') ]
        end

        it { expect(subject.recommendations_for(comics)).to eq [comics[0], comics[2]] }
      end

      context "all have been removed from storage" do
        let(:comics) do
          [ build_comic(removed: '1-10'), build_comic(removed: '1-3,5,9'), build_comic(removed: '5') ]
        end

        it { expect(subject.recommendations_for(comics)).to eq comics }
      end
    end
  end
end
