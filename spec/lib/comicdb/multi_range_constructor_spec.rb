require 'rails_helper'

RSpec.describe Comicdb::MultiRangeConstructor do
  subject(:constructor) { described_class.new(input_ints) }

  describe "#multi_range" do
    context "When the input is invalid" do
      context "when we have letters in array" do
        let(:input_ints) { [1, "a"] }

        it "raises an ArgumentError" do
          expect { constructor.multi_range }.
            to raise_error(ArgumentError, /Multi-ranges must only contain positive whole numbers/)
        end
      end

      context "When we have a negative number" do
        let(:input_ints) { [3, -1] }

        it "raises an ArgumentError" do
          expect { constructor.multi_range }.
            to raise_error(ArgumentError, /Multi-ranges must only contain positive whole numbers/)
        end
      end

      context "When we have a float" do
        let(:input_ints) { [3.1, 4] }

        it "raises an ArgumentError" do
          expect { constructor.multi_range }.
            to raise_error(ArgumentError, /Multi-ranges must only contain positive whole numbers/)
        end
      end
    end

    context "When the input is empty" do
      let(:input_ints) { [] }

      it { expect(constructor.multi_range).to eq "" }
    end

    context "When the multi-range is a single int" do
      let(:input_ints) { [4] }

      it { expect(constructor.multi_range).to eq "4" }
    end

    context "When the multi-range is a list of ints" do
      let(:input_ints) { [4, 6, 9] }

      it { expect(constructor.multi_range).to eq "4,6,9" }
    end

    context "When the multi-range is a single range" do
      let(:input_ints) { [4, 5, 6, 7] }

      it { expect(constructor.multi_range).to eq "4-7" }
    end

    context "When the multi-range is a list of ranges" do
      let(:input_ints) { [4, 5, 6, 8, 9, 10] }

      it { expect(constructor.multi_range).to eq "4-6,8-10"  }
    end

    context "when a number is repeated multiple times duplicates are eliminated" do
      let(:input_ints) { [4, 8, 9, 9, 9, 10, 11, 11, 12, 13] }

      it { expect(constructor.multi_range).to eq "4,8-13" }
    end

    context "When the numbers are out of order they are sorted" do
      let(:input_ints) { [8, 10, 9, 3, 1, 4] }

      it { expect(constructor.multi_range).to eq "1,3-4,8-10" }
    end
  end
end
