require "rails_helper"

RSpec.describe Comicdb::ComicStatistics do
  subject { described_class.new(comics) }

  def build_comic(has_issue_zero: false, issues_owned: '1-10', next_issue_to_read: 1, issues_removed_from_storage: '')
    FactoryBot.build(
      :comic,
      has_issue_zero: has_issue_zero,
      issues_owned: issues_owned,
      total_issues: 10,
      next_issue_to_read: next_issue_to_read,
      issues_removed_from_storage: issues_removed_from_storage
    )
  end

  context "#number_of_comics_collected" do
    context "with no comics collected" do
      let(:comics) { [] }

      it { expect(subject.number_of_comics_collected).to eq 0 }
    end

    context "with one comic collected" do
      let(:comics) { [build_comic] }

      it { expect(subject.number_of_comics_collected).to eq 1 }
    end

    context "with two comics collected" do
      let(:comics) { [build_comic, build_comic] }

      it { expect(subject.number_of_comics_collected).to eq 2 }
    end
  end

  context "#total_issues_owned" do
    context "with no comics owned" do
      let(:comics) { [] }

      it { expect(subject.total_issues_owned).to eq 0 }
    end

    context "with one comic owned" do
      let(:comics) { [build_comic(has_issue_zero: has_issue_zero, issues_owned: issues_owned)] }

      context "and that comic has an issue zero" do
        let(:has_issue_zero) { true }

        context "and that issues is owned" do
          let(:issues_owned) { '0-10' }

          it { expect(subject.total_issues_owned).to eq 11 }
        end

        context "and that issue is not owned" do
          let(:issues_owned) { '1-10' }

          it { expect(subject.total_issues_owned).to eq 10 }
        end
      end

      context "and that comic does not have an issue zero" do
        let(:has_issue_zero) { false }

        context "and all the issues are owned" do
          let(:issues_owned) { '1-10' }

          it { expect(subject.total_issues_owned).to eq 10 }
        end

        context "and only some of the issues are owned" do
          let(:issues_owned) { '2-4,6,8-10' }

          it { expect(subject.total_issues_owned).to eq 7 }
        end

        context "and none of the issues are owned" do
          let(:issues_owned) { '' }

          it { expect(subject.total_issues_owned).to eq 0 }
        end
      end
    end

    context "with two comics owned" do
      context "and all of the issues are owned in both" do
        let(:comics) { [build_comic, build_comic] }

        it { expect(subject.total_issues_owned).to eq 20 }
      end

      context "and some of the issues are owned in one and all in the other" do
        let(:comics) { [build_comic, build_comic(issues_owned: '6-10')] }

        it { expect(subject.total_issues_owned).to eq 15 }
      end

      context "and not all of the issues are owned in either comic" do
        let(:comics) { [build_comic(issues_owned: '4-7'), build_comic(issues_owned: '6-10')] }

        it { expect(subject.total_issues_owned).to eq 9 }
      end

      context "and none of the issues are owned in one comic, but some are owned in the other" do
        let(:comics) { [build_comic(issues_owned: ''), build_comic(issues_owned: '6-10')] }

        it { expect(subject.total_issues_owned).to eq 5 }
      end

      context "and none of the issues are owned in either comic" do
        let(:comics) { [build_comic(issues_owned: ''), build_comic(issues_owned: '')] }

        it { expect(subject.total_issues_owned).to eq 0 }
      end
    end
  end

  context "#total_issues_read" do
    context "where we have no comics" do
      let(:comics) { [] }

      it { expect(subject.total_issues_read).to eq 0 }
    end

    context "where we have one comic" do
      let(:comics) { [build_comic(next_issue_to_read: 6)] }

      it { expect(subject.total_issues_read).to eq 5 }
    end

    context "where we have two comics" do
      let(:comics) do
        [
          build_comic(next_issue_to_read: 6),
          build_comic(has_issue_zero: true, next_issue_to_read: 6, issues_owned: '0-10')
        ]
      end

      it { expect(subject.total_issues_read).to eq 11 }
    end
  end

  context "#total_issues_removed_from_storage" do
    context "with no comics" do
      let(:comics) { [] }

      it { expect(subject.total_issues_removed_from_storage).to eq 0 }
    end

    context "with only one comic" do
      context "and that comic has an issue zero" do
        context "and issue zero is removed from storage" do
          let(:comics) { [build_comic(has_issue_zero: true, issues_removed_from_storage: '0-2')] }

          it { expect(subject.total_issues_removed_from_storage).to eq 3 }
        end

        context "and issue zero is not removed from storage" do
          let(:comics) { [build_comic(has_issue_zero: true, issues_removed_from_storage: '1-2')] }

          it { expect(subject.total_issues_removed_from_storage).to eq 2 }
        end
      end

      context "and that comic does not have an issue zero" do
        context "and it has some issues removed from storage" do
          let(:comics) { [build_comic(issues_removed_from_storage: '5,7-9')] }

          it { expect(subject.total_issues_removed_from_storage).to eq 4 }
        end

        context "and it has no issues removed from storage" do
          let(:comics) { [build_comic] }

          it { expect(subject.total_issues_removed_from_storage).to eq 0 }
        end
      end
    end

    context "with two comics" do
      context "when both series have issues removed from storage" do
        let(:comics) do
          [
            build_comic(issues_removed_from_storage: '5,7-9'),
            build_comic(issues_removed_from_storage: '1,4,5')
          ]
        end

        it { expect(subject.total_issues_removed_from_storage).to eq 7 }
      end

      context "when only one has issues removed from storage" do
        let(:comics) do
          [
            build_comic(issues_removed_from_storage: '5,7-9'),
            build_comic(issues_removed_from_storage: '')
          ]
        end

        it { expect(subject.total_issues_removed_from_storage).to eq 4 }
      end

      context "when neither has issues removed from storage" do
        let(:comics) do
          [
            build_comic(issues_removed_from_storage: ''),
            build_comic(issues_removed_from_storage: '')
          ]
        end

        it { expect(subject.total_issues_removed_from_storage).to eq 0 }
      end
    end
  end
end
