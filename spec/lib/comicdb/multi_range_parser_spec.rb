require 'rails_helper'

RSpec.describe Comicdb::MultiRangeParser do
  subject(:parser) { described_class.new(multi_range_string) }

  describe "#expanded_numbers" do
    context "When the input is invalid" do
      context "when we have letters in the multi-range" do
        let(:multi_range_string) { "1,a" }

        it "raises a MultiRangeInvalidError" do
          expect { parser.expanded_numbers }.
            to raise_error(Comicdb::MultiRangeInvalidError, /not either an integer.*or a range/)
        end
      end

      context "when the upper  bound of a range is below the lower bound" do
        let(:multi_range_string) { "10-5" }

        it do
          expect { parser.expanded_numbers }.
            to raise_error(Comicdb::MultiRangeInvalidError, /must be lower than upper bound/)
        end
      end
    end

    context "When the multi-range is empty" do
      context "and the input is an empty string" do
        let(:multi_range_string) { "" }

        it { expect(parser.expanded_numbers).to eq [] }
      end

      context "and the input is nil" do
        let(:multi_range_string) { nil }

        it { expect(parser.expanded_numbers).to eq [] }
      end
    end

    context "When the multi-range is a single int" do
      let(:multi_range_string) { "4" }

      it { expect(parser.expanded_numbers).to eq [4] }
    end

    context "When the multi-range is a list of ints" do
      let(:multi_range_string) { "4,6,9" }

      it { expect(parser.expanded_numbers).to eq [4, 6, 9] }
    end

    context "When the multi-range is a single range" do
      let(:multi_range_string) { "4-7" }

      it { expect(parser.expanded_numbers).to eq [4, 5, 6, 7] }
    end

    context "When the multi-range is a list of ranges" do
      let(:multi_range_string) { "4-6,8-10" }

      it { expect(parser.expanded_numbers).to eq [4, 5, 6, 8, 9, 10] }
    end

    context "When the multi-range is a mix of ints and ranges" do
      let(:multi_range_string) { "4,8-10,12-13,18" }

      it { expect(parser.expanded_numbers).to eq [4, 8, 9, 10, 12, 13, 18] }
    end

    context "when a number is repeated multiple times duplicates are eliminated" do
      let(:multi_range_string) { "4,4,4,8-10,9-13" }

      it { expect(parser.expanded_numbers).to eq [4, 8, 9, 10, 11, 12, 13] }
    end

    context "When the numbers are out of order they are sorted" do
      let(:multi_range_string) { "8-10,4,1,3" }

      it { expect(parser.expanded_numbers).to eq [1, 3, 4, 8, 9, 10] }
    end
  end
end
