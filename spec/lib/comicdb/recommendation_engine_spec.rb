require 'rails_helper'

class FirstTwoRecommender
  attr_reader :name

  def initialize
    @name = :first_two
  end

  def recommendations_for(comics)
    comics.first(2)
  end
end

class EvenRecommender
  attr_reader :name

  def initialize
    @name = :even_elements
  end

  def recommendations_for(comics)
    comics.each_with_index.select { |comic, i| i.even? }.map(&:first)
  end

end

RSpec.describe Comicdb::RecommendationEngine do
  subject(:engine) do
    described_class.new(recommenders, comics)
  end
  let(:comics) do
    [
      FactoryBot.build(:comic),
      FactoryBot.build(:comic),
      FactoryBot.build(:comic),
      FactoryBot.build(:comic)
    ]
  end

  describe "#recommendations" do
    context "With one recommender" do
      let(:recommenders) { [FirstTwoRecommender.new] }

      it "returns the expected result" do
        expect(engine.recommendations).to eq(
          {
            recommenders[0] => [comics[0], comics[1]]
          }
        )
      end
    end

    context "with two recommenders" do
      let(:recommenders) { [FirstTwoRecommender.new, EvenRecommender.new] }

      it "returns the expected result" do
        expect(engine.recommendations).to eq(
          {
            recommenders[0] => [comics[0], comics[1]],
            recommenders[1] => [comics[0], comics[2]]
          }
        )
      end
    end
  end
end
