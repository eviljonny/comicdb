module Api
  module V1
    # Comics controller for the API
    class ComicsController < Api::V1::ApplicationController
      before_action :authenticate_user_from_token!
      before_action :authenticate_user!

      respond_to :json

      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

      def index
        respond_with current_user.comics.order(:title).all
      end

      def show
        respond_with current_user.comics.find(params[:id])
      end

      private

      def record_not_found(_error)
        render json: { error: 'Comic not found' }, status: :not_found
      end

      def authenticate_user_from_token!
        email = request.headers['X-User-Email'].presence
        user = email && User.find_by_email(email)

        token = request.headers['X-User-Token'].presence

        # Don't even attempt to auth anyone if they have not provided a token
        return if token.blank?

        # Notice how we use Devise.secure_compare to compare the token
        # in the database with the token given in the params, mitigating
        # timing attacks.
        sign_in user, store: false if user && token && Devise.secure_compare(user.authentication_token, token)
      end
    end
  end
end
