module Api
  module V1
    # Base controller for the API
    class ApplicationController < ActionController::Base
      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      protect_from_forgery with: :null_session

      rescue_from Exception do |exception|
        error = { message: exception.message }
        render json: error
      end
    end
  end
end
