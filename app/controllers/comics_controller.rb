# This controller deals with all actions related to the Comic model
class ComicsController < ApplicationController
  before_action :authenticate_user!

  def index
    @comics = current_user.comics.order(:title).all
  end

  def show
    @comic = current_user.comics.find(params[:id])
  end

  def new
    @comic = current_user.comics.new
  end

  def edit
    @comic = current_user.comics.find(params[:id])
  end

  def create
    @comic = current_user.comics.new(comic_params)

    if @comic.save
      redirect_to @comic
    else
      render 'new'
    end
  end

  def update
    @comic = current_user.comics.find(params[:id])

    if @comic.update(comic_params)
      redirect_to @comic
    else
      render 'edit'
    end
  end

  def destroy
    current_user.comics.find(params[:id]).destroy

    redirect_to comics_path
  end

  private

  def comic_params
    params.require(:comic).permit(
      :title, :total_issues, :url, :issues_owned, :issues_removed_from_storage,
      :next_issue_to_read, :has_issue_zero, :no_more_issues,
      :notes
    )
  end
end
