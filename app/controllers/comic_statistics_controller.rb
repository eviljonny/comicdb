# Deals with all actions related to displaying a users comic statistics
class ComicStatisticsController < ApplicationController
  before_action :authenticate_user!

  def index
    @stats = Comicdb::ComicStatistics.new(current_user.comics)
  end
end
