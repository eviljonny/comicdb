# Base controller for the UI side of the app
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # We need to custom override this, when the application is run from a sub-
  # folder devise incorrectly redirects, this fixes that incorrect redirecting
  # for users who are not signed in.
  def authenticate_user!(*args, **kwargs)
    if user_signed_in?
      super(*args, **kwargs)
    else
      redirect_to new_user_session_path
    end
  end
end
