class RecommendationsController < ApplicationController
  before_action :authenticate_user!

  def index
    recommender = Comicdb::RecommendationEngine.new(
      [
        Comicdb::Recommenders::NextInSeriesRecommender.new,
        Comicdb::Recommenders::CompleteUnstartedSeriesRecommender.new,
        Comicdb::Recommenders::IssuesRemovedFromStorageRecommender.new,
        Comicdb::Recommenders::MissingIssuesRecommender.new
      ],
      current_user.comics
    )
    @recommendations = recommender.recommendations
  end
end
