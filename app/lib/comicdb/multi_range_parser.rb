module Comicdb
  MultiRangeInvalidError = Class.new(StandardError)

  # Takes a multi range string, made up of ints and ranges (with a '-' separator) and expands
  # them out into a fully realised array with every int that was covered by the multi-range.
  #
  # All ranges are inclusive
  class MultiRangeParser
    def initialize(multi_range_string)
      @multi_range_string = (multi_range_string.nil? ? "" : multi_range_string)
    end

    def expanded_numbers
      @expanded_numbers ||= @multi_range_string.split(',').each_with_object(Set.new) do |range, set|
        case range
        when /\A[0-9]+\z/ # A number
          set.add(range.to_i)
        when /\A[0-9]+-[0-9]+\z/ # A range
          (parse_range(range)).each { |issue| set.add issue }
        else
          raise MultiRangeInvalidError, "Value [#{range}] not either an integer (e.g. 3), or a range (e.g 10-14)"
        end
      end.sort
    end

    private

    def parse_range(range)
      nums = range.split '-'
      lower = nums[0].to_i
      upper = nums[1].to_i

      if lower >= upper
        raise MultiRangeInvalidError, "Lower bound in range [#{range}] must be lower than upper bound"
      end

      lower..upper
    end
  end
end
