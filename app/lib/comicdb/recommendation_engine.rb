module Comicdb
  # Orchestrates a series of recommenders to produce a list of recomendations.
  #
  # Pass in a series of recommenders, you can then access recommendations.
  #
  # Recommendations is a hash with the recommender as the key and
  # it's recomemendations as the value
  class RecommendationEngine
    attr_reader :recommendations

    def initialize(recommenders, comics)
      @recommenders = recommenders
      @comics = comics

      @recommendations = generate_recommendations
    end

    private

    def generate_recommendations
      @recommenders.each_with_object({}) do |recommender, acc|
        acc[recommender] = recommender.recommendations_for(@comics)
      end
    end
  end
end
