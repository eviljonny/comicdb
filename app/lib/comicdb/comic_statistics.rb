module Comicdb
  # Generates statistics about a collection of comics
  class ComicStatistics
    extend Forwardable

    def initialize(comics)
      @comics = comics
    end

    def_delegator :@comics, :size, :number_of_comics_collected

    def total_issues_owned
      @comics.reduce(0) do |acc, comic|
        acc + comic.expanded_issues_owned.size
      end
    end

    def total_issues_read
      @comics.reduce(0) do |acc, comic|
        acc + comic.number_of_issues_read
      end
    end

    def total_issues_removed_from_storage
      @comics.reduce(0) do |acc, comic|
        acc + comic.expanded_issues_removed_from_storage.size
      end
    end
  end
end
