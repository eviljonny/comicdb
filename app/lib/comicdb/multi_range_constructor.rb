module Comicdb
  # Takes an array of ints and constructs a multi-range from it. A multi-range is a
  # string, made up of ints and ranges (with a '-' separator).
  #
  # All ranges are inclusive
  class MultiRangeConstructor
    def initialize(ints)
      ints.each do |i|
        if !i.is_a?(Fixnum) || i < 0
          raise ArgumentError, "Multi-ranges must only contain positive whole numbers"
        end
      end

      @complete_number_list = ints.sort
    end

    def multi_range
      @multi_range_string ||= generate_multi_range.join(",")
    end

    private

    def generate_multi_range
      collapsed = []

      min = nil

      @complete_number_list.each_cons(2) do |cur, nxt|
        min = cur if min.nil?

        next unless nxt > cur + 1

        collapsed << generate_multi_range_string(min, cur)
        min = nil
      end

      collapsed << generate_multi_range_string(min, @complete_number_list.last)
    end

    def generate_multi_range_string(min, current)
      min == current || min.nil? ? current.to_s : "#{min}-#{current}"
    end
  end
end
