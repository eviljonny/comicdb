module Comicdb
  module Recommenders
    # Selects comics which have issues that have been removed from
    # storage
    class IssuesRemovedFromStorageRecommender
      attr_reader :name

      def initialize
        @name = :issues_removed_from_storage
      end

      def recommendations_for(comics)
        comics.select(&:any_issues_removed_from_storage?)
      end
    end
  end
end
