module Comicdb
  module Recommenders
    # Selects comics which have issues missing (i.e. where not all issues of a series
    # are owned by the user)
    class MissingIssuesRecommender
      attr_reader :name

      def initialize
        @name = :missing_issues
      end

      def recommendations_for(comics)
        comics.reject(&:all_issues_owned?)
      end
    end
  end
end
