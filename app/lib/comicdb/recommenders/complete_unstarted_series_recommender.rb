module Comicdb
  module Recommenders
    # Selects comics where the series is complete (no more issues, and all issues are owned),
    # and has not started to be read yet
    class CompleteUnstartedSeriesRecommender
      attr_reader :name

      def initialize
        @name = :complete_unstarted_series
      end

      def recommendations_for(comics)
        comics.select do |comic|
          comic.no_more_issues && comic.all_issues_owned? && !comic.started_reading?
        end
      end
    end
  end
end
