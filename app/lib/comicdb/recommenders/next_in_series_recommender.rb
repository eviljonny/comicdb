module Comicdb
  module Recommenders
    # Selects series where the user has started reading it and the next comic
    # in the series is available (exists and is owned by the user)
    class NextInSeriesRecommender
      attr_reader :name

      def initialize
        @name = :next_in_series
      end

      def recommendations_for(comics)
        comics.select do |comic|
          comic.started_reading? && comic.next_issue_available?
        end
      end
    end
  end
end
