# Represents a comic series (with a single title, so spinoffs and specials
# would be a separate comic) for a single user
class Comic < ActiveRecord::Base
  belongs_to :user

  validates :title, presence: true, length: { minimum: 1 }
  validates :total_issues, presence: true, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
  validates :next_issue_to_read, presence: true, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
  validates :has_issue_zero, inclusion: [true, false]
  validates :no_more_issues, inclusion: [true, false]
  validate :must_be_valid_issues_owned, :highest_issue_owned_less_than_total_issues,
    :must_be_valid_issues_removed, :missing_from_storage_must_be_only_issues_owned

  def expanded_issues_owned
    @expanded_issues_owned ||= Comicdb::MultiRangeParser.new(issues_owned).expanded_numbers
  end

  def expanded_issues_removed_from_storage
    @expanded_issues_removed_from_storage ||= Comicdb::MultiRangeParser.new(issues_removed_from_storage).expanded_numbers
  end

  def missing_issues
    @collapsed_missing_issues ||= Comicdb::MultiRangeConstructor.new(all_missing_issues).multi_range
  end

  def all_missing_issues
    @all_missing_issues ||= (first_issue_in_series..total_issues).each_with_object([]) do |issue, missing|
      missing << issue unless expanded_issues_owned.include? issue
    end
  end

  def started_reading?
    return false if no_issues_owned?

    first_issue_owned < next_issue_to_read
  end

  def finished_reading?
    return false if no_issues_owned?

    next_issue_to_read > total_issues
  end

  def more_to_read?
    return false if no_issues_owned?

    next_issue_to_read <= last_issue_owned
  end

  def first_issue_in_series
    has_issue_zero? ? 0 : 1
  end

  def first_issue_owned
    return -1 if no_issues_owned?

    expanded_issues_owned.first
  end

  def last_issue_owned
    return -1 if no_issues_owned?

    expanded_issues_owned.last
  end

  def all_issues_owned?
    return false if no_issues_owned?

    all_missing_issues.empty?
  end

  def next_issue_available?
    return false if no_issues_owned?

    next_issue = next_issue_to_read

    next_issue = first_issue_in_series if next_issue < first_issue_in_series

    expanded_issues_owned.include? next_issue
  end

  def any_issues_removed_from_storage?
    !expanded_issues_removed_from_storage.empty?
  end

  def number_of_issues_read
    issues_owned_lower_than_next_to_read = expanded_issues_owned.select { |i| i < next_issue_to_read }
    issues_owned_lower_than_next_to_read.size
  end

  private

  def no_issues_owned?
    issues_owned.blank?
  end

  def must_be_valid_issues_owned
    expanded_issues_owned
  rescue Comicdb::MultiRangeInvalidError => e
    errors.add(:issues_owned, "Invalid listing of issues owned, #{e.message}")
  end

  def total_issues_nil?
    if total_issues.nil?
      errors.add(:total_issues, 'total issues can not be nil')
      return true
    end

    false
  end

  def highest_issue_owned_less_than_total_issues
    return if total_issues_nil?

    return if issues_owned.blank?

    max_issue_owned = expanded_issues_owned.max

    return if total_issues >= max_issue_owned

    errors.add(:issues_owned, "max value [#{max_issue_owned}] greater than total issues")
  rescue Comicdb::MultiRangeInvalidError => e
    errors.add(:issues_owned, "Invalid listing of issues owned, #{e.message}")
  end

  def must_be_valid_issues_removed
    expanded_issues_removed_from_storage
  rescue Comicdb::MultiRangeInvalidError => e
    errors.add(:issues_removed_from_storage, "Invalid listing of issues removed from storage, #{e.message}")
  end

  def missing_from_storage_must_be_only_issues_owned
    missing_from_storage_but_not_owned = (expanded_issues_removed_from_storage - expanded_issues_owned)

    return if missing_from_storage_but_not_owned.empty?

    missing_issues_range = Comicdb::MultiRangeConstructor.new(missing_from_storage_but_not_owned).multi_range

    errors.add(
      :issues_removed_from_storage,
      "All issues missing from storage must be owned, issues #{missing_issues_range} are not owned " \
        "but have been listed as missing from storage"
    )
  rescue Comicdb::MultiRangeInvalidError => e
    errors.add(:issues_removed_from_storage, "Invalid listing of issues removed from storage, #{e.message}")
  end
end
