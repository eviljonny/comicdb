# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $(".comics-list").dataTable({
    "aoColumns": [
      null, # Title
      null, # Total Issues
      null, # Next Issue to read
      { "bSortable": false }, # Issues Owned
      { "bSortable": false }, # Status
      { "bSortable": false }  # Actions
    ]
  })
