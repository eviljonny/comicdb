class AddNextIssueToReadToComics < ActiveRecord::Migration
  def change
    add_column :comics, :next_issue_to_read, :integer, default: 0
  end
end
