class AddIssuesRemovedFromStorageToComic < ActiveRecord::Migration[5.0]
  def change
    add_column :comics, :issues_removed_from_storage, :string
  end
end
