class AddDefaultValueToTotalIssues < ActiveRecord::Migration
  def up
      change_column :comics, :total_issues, :integer, default: 0
      Comic.find_each do |comic|
        if comic.total_issues.blank?
          comic.total_issues = 0
          comic.save!
        end
      end
  end

  def down
      change_column :comics, :total_issues, :integer, default: nil
  end
end
