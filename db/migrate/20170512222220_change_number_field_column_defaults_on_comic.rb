class ChangeNumberFieldColumnDefaultsOnComic < ActiveRecord::Migration[5.0]
  def up
    change_column :comics, :total_issues, :integer, default: 1
    change_column :comics, :next_issue_to_read, :integer, default: 1
  end

  def down
    change_column :comics, :total_issues, :integer, default: 0
    change_column :comics, :next_issue_to_read, :integer, default: 0
  end
end
