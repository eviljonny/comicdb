class AddNoMoreIssuesToComics < ActiveRecord::Migration
  def change
    add_column :comics, :no_more_issues, :boolean, default: false, null: false
  end
end
