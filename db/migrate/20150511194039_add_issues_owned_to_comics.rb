class AddIssuesOwnedToComics < ActiveRecord::Migration
  def change
    add_column :comics, :issues_owned, :string
  end
end
