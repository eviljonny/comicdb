class AddNotesToComics < ActiveRecord::Migration
  def change
    add_column :comics, :notes, :text
  end
end
