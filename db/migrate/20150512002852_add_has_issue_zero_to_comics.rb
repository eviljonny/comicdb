class AddHasIssueZeroToComics < ActiveRecord::Migration
  def change
    add_column :comics, :has_issue_zero, :boolean, default: false
  end
end
