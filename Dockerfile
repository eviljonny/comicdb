From ruby:2.7.1-slim

MAINTAINER Jonathan Harden <jfharden@gmail.com>

RUN apt-get update && apt-get install -qq -y --fix-missing --no-install-recommends \
  build-essential \
  git \
  libpq-dev 

RUN adduser --disabled-password --gecos '' comicdb

ENV INSTALL_PATH /comicdb
ENV RAILS_ENV production
# ENV RAILS_DEVISE_SECRET_KEY='jah3roe2lah6oow7ahLeeTheixa2ooco9eishureXoh5ish1libah3Hai8kohdee5yut0ohL7bai5neemai2ju6Siughieshahc5ToGieng9Vaixo3jeiFaiGaiD1phu'
# ENV SECRET_KEY_BASE='wohs0shei4eboegh5kahshohxieVaeng3geeYi5oot2koufichei1Vaephah1pohsh9wugi5Hea0phiem1auf7Roo4ooquahguCu'

RUN mkdir -p $INSTALL_PATH && chown -R comicdb:comicdb $INSTALL_PATH

USER comicdb

RUN gem install bundler --version "=2.1.4"

WORKDIR $INSTALL_PATH

ADD Gemfile $INSTALL_PATH/Gemfile
ADD Gemfile.lock $INSTALL_PATH/Gemfile.lock

RUN bundle install

ADD . $INSTALL_PATH

USER root

RUN chown -R comicdb:comicdb $INSTALL_PATH

USER comicdb

# These pretend secrets are just to make the build work, in real production it will be overridden by the appropriate env var
RUN RAILS_DEVISE_SECRET_KEY=foo SECRET_KEY_BASE=bar bundle exec rake assets:precompile

EXPOSE 3000

CMD ["bin/puma"]
