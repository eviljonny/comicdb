Comic DB
========

This is my first from-scratch simple rails app. It should be able to keep track of what comics I have, where I am up to
and what might be missing from my collection.

License
=======

This project is distributed under the MIT license (See LICENSE.txt in the project root for full conditions).

Run up a local dev instance
===========================

For the very first time
-----------------------

Note, you will need to enter the passwords for comicdb\_dev and comicdb\_test during this process. These are:

```
docker-compose up -d
docker-compose run comicdb rake db:create
docker-compose run comicdb rake db:migrate
```

Every further time
------------------

```
docker-compose up -d
```

Running the full test suite
---------------------------

```
docker-compose -f docker-compose.tests.yml run comicdb-tests
```

Restarting the app (to pick up changes which require restart)
-------------------------------------------------------------

```
docker-compose restart comicdb
```

Running in production
=====================

You need to provide the following ENV vars:

* RAILS\_COMICDB\_DB\_HOST\_PROD - Production database host
* RAILS\_COMICDB\_DB\_NAME\_PROD - Production database database name
* RAILS\_COMICDB\_DB\_USER\_PROD - Production database username
* RAILS\_COMICDB\_DB\_PASS\_PROD - Production database password

* RAILS\_SECRET\_KEY\_BASE - The secret key base for rails
* RAILS\_DEVISE\_SECRET\_KEY - The secret key for devise to generate email tokens etc

* RAILS\_EMAIL\_BASE\_URL - URL to use in emails to form hyperlinks
* RAILS\_EMAIL\_FROM\_ADDRESS - FROM address for emails sent
* RAILS\_EMAIL\_REPLY\_TO\_ADDRESS - REPLY TO address for emails sent
* RAILS\_SMTP\_SERVER - SMTP mail server host
* RAILS\_SMTP\_PORT - SMTP mail server port
* RAILS\_SMTP\_USER - SMTP mail server user
* RAILS\_SMTP\_PASS - SMTP mail server password

* RAILS\_SERVE\_STATIC\_FILES - Set this to true to serve the static pre-compiled assets.
  You NEED this for producition env
